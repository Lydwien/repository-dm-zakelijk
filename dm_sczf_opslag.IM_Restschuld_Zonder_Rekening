/*
********************************** versiebeheer *******************************************************************************
datum                     versie          	naam                opmerkingen
18-03-2021				  bo-4618		  	marcel venrooy	  	- aanpassing code zodat enkel de laatst geladen maand wordt getoond
																tevens tonen van actuele data voor de velden uit het stermodel incl. peildatum hiervan
31-03-2022				  tb-3550			marcel venrooy		koppeling naar actuele gegevens dwh gemaakt t.b.v. accountmanager en klantleider
19-10-2022				  pzcs-3840			lydwien batterink	- data die eerder uit scz_revisies_mnd werd opgehaald komt nu rechtstreeks uit het DWH. 
																  Dit ivm een eerdere aanpassing op de scope van DM Zakelijk die te strikt is voor doeleinden van deze view
																  (Er is bewust voor gekozen om restschulden niet langer mee te nemen in de DM)
																- bank_nr toegevoegd
*******************************************************************************************************************************

rapporteigenaar: Team FUNdamentals, Hub Ondernemen
*/


replace view dm_sczf_opslag.im_restschuld_zonder_rekening as

select lvm.peil_dt as jaarmaand
, sk.BANK_NR as bank_nr --toegevoegd in pzcs-3840
, cast(ph.bron_partij_nr as decimal(7)) as clientnummer
, sk.naam as naam_relatie
, skl.klant_nr as klantleider_nr
, NULL as klantvolger_nr -- hoeft niet te worden getoond
, bed.accountmanager
, cast(cast(rev.contract_agendering_dt as char(10)) as date format 'yyyymmdd') as revisiedatum
, rev.rente_revisie_methode_cd as sleutel_cd
, dm.decode_omschr as sleutel
, sk.risicoklasse_cd as kredietklasse
, ster.date_act as inzicht_dt_act
, ster.overtrekking_bg as overtrekking_bg_act
, ster.kredietlimiet_bg as kredietlimiet_bg_act
, ster.rekening_nr as rekening_nr_act

from dm_sczf_opslag.laatst_verwerkte_maand lvm

-- onderstaande toegevoegd in pzcs-3840

join dwh.sas_klant sk  
on sk.begin_dt <= lvm.inzicht_dt
and sk.eind_dt > lvm.inzicht_dt  
and sk.geldig_ind = 1

join dwh.partij_h ph
on sk.partij_oid = ph.partij_oid

join dwh.sas_klant_rte_revisie_agenderi rev
on rev.partij_oid = sk.partij_oid
and rev.begin_dt <= lvm.inzicht_dt
and rev.eind_dt > lvm.inzicht_dt
and rev.effectief_dt <= lvm.inzicht_dto 
and rev.afloop_dt >  lvm.inzicht_dto
and rev.geldig_ind = 1
and rev.contract_agendering_dt between 20100101 and 20801231 -- contract_agendering_dt kan de waarde 99999999 bevatten. Omdat de
															 -- contract_agendering_dt in de select wordt geCAST naar DATE FORMAT
															 -- loopt de query zonder deze filtering vast. 
															 
left outer join dwh.sas_decode dm
on dm.decode_num_cd = rev.rente_revisie_methode_cd
and dm.def_srt = 1532
and dm.eind_dt = '9999-12-31'
and dm.geldig_ind = 1

-- Bepalen van het gekoppelde kantoor en bank
join dwh.partij_partij_l ppl_kan
on ppl_kan.gerelateerde_partij_oid = sk.partij_oid
and ppl_kan.partij_rol_cd = 1
and ppl_kan.eind_dt > lvm.inzicht_dt
and ppl_kan.begin_dt <= lvm.inzicht_dt
and ppl_kan.geldig_ind = 1
and (ppl_kan.relatie_eind_dt is null or ppl_kan.relatie_eind_dt > lvm.inzicht_dt)
and ppl_kan.relatie_begin_dt <= lvm.inzicht_dt

left outer join dwh.sas_kantoorhierarchie kan
on kan.partij_oid = ppl_kan.relateert_aan_partij_oid
and kan.begin_dt <= lvm.inzicht_dt
and kan.eind_dt > lvm.inzicht_dt
and kan.geldig_ind = 1

-- einde toegevoegd in pzcs-3840

left join dwh.partij_partij_l ppl
on ppl.gerelateerde_partij_oid = sk.partij_oid --leider
and ppl.partij_rol_cd = 10 
and ppl.geldig_ind = 1 
and ppl.eind_dt = '9999-12-31'
and (ppl.relatie_eind_dt > date or ppl.relatie_eind_dt is null)		
				
left join  dwh.sas_klant skl -- tbv gegevens klantleider
on skl.partij_oid = ppl.relateert_aan_partij_oid
and skl.eind_dt = '9999-12-31'
and skl.geldig_ind = 1

-- ophalen van de naam van een accountmanager
left outer join       
     ( 
       --dezelfde bediende heeft in sas en kis verschillende bediendenummers. scz gebruikt het bediendenr uit sas_bediende. 
       --in sas ontbreken echter de naamgegevens. daarom wordt er hier ook gekoppeld aan kis_bediende. 
         select      
         sb.bediende_nr      
         , sb.bank_nr 
         , kb.achternaam || case when  kb.tussenvoegsel is null then '' else  ', ' || kb.tussenvoegsel end as accountmanager 
         from dwh.sas_bediende sb   
              
         join dwh.partij_partij_l ppl      
         on sb.partij_oid = ppl.relateert_aan_partij_oid   
         and ppl.partij_rol_cd = 15 -- = relatie tussen een sas-bediende en een kis-bediende. 
         and ppl.eind_dt = '9999-12-31'
         and ppl.geldig_ind = 1
         and ppl.relatie_eind_dt is null
              
         join dwh.kis_bediende kb   
         on kb.partij_oid = ppl.gerelateerde_partij_oid   
         and kb.eind_dt = '9999-12-31'
         and kb.geldig_ind = 1
         and kb.actief_ind = 'a'
         
         where sb.geldig_ind = 1
         and  sb.eind_dt = '9999-12-31'
         and sb.actueel_ind = 1 
         
         -- filteren op meest recente bediende_nr per bank. 
         qualify row_number() over (partition by sb.bank_nr,sb.bediende_nr  order by sb.begin_dt desc) = 1
      ) bed  
on bed.bediende_nr = sk.account_manager_nr      
and bed.bank_nr = sk.bank_nr     


left join 
	(
		select 
		max(fr.dim_datum_id)over(partition by sas_klant_nr) as date_act, --meest actuele datum stermodel ophalen
		fr.dim_datum_id,
		dk.sas_klant_nr, 
		fr.kredietlimiet_bg, 
		fr.overtrekking_bg,
		dr.afbetaald_ind,
		dr.rekening_nr 
	
		from dm_ster.feit_rekening fr
		
		join dm_ster.dim_rekening dr
		on dr.dim_rekening_id = fr.dim_rekening_id
	
		join dm_ster.dim_klant dk
		on dk.dim_klant_id = fr.dim_klant_id
	
		join dm_ster.dim_product pr
		on pr.dim_product_id = fr.dim_product_id
	
		--ophalen (en tonen van) de meest actuele data uit het stermodel
		where fr.dim_datum_id > current_date-7 --we mogen ervan uitgaan dat het stermodel minder dan 6 dagen achterloopt
		and (pr.product_nr_peil in (947, 948) --restschuld
		or (pr.product_nr_peil in (82, 83) and dr.afbetaald_ind = 0))
		
		qualify date_act = fr.dim_datum_id -- de laatste datum aanwezig in het stermodel (feit_rekening tabel) wordt opgehaald
	)ster								
on ster.sas_klant_nr = CAST(ph.bron_partij_nr AS DECIMAL(7))

where kan.kantoor_nr = 139 --cbk
and sk.risicoklasse_cd = 13 -- selectie aangegeven door stakeholder. betekenis onbekend
and rev.rente_revisie_methode_cd in (53,54,55,56,57,58,59);
 